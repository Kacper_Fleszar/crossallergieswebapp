package pl.lodz.p.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import pl.lodz.p.enums.PollenSubType;

@Component
public class PollenSubTypeConverter implements Converter<String, PollenSubType> {

    @Override
    public PollenSubType convert(String s) {
        if (s == null) {
            return null;
        }
        return PollenSubType.valueOf(s.toUpperCase());
    }
}
