package pl.lodz.p.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import pl.lodz.p.enums.PollenType;

@Component
public class PollenTypeConverter implements Converter<String, PollenType> {

    @Override
    public PollenType convert(String s) {
        if (s == null) {
            return null;
        }
        return PollenType.valueOf(s.toUpperCase());
    }
}
