package pl.lodz.p.entities;

import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import pl.lodz.p.helpers.ApiKeyHelper;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Getter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Entity
@SQLDelete(sql = "UPDATE api_key_entity SET removed = 1 WHERE api_key_id = ?")
@Where(clause = "removed = 0")
public class ApiKeyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long apiKeyId;

    @NotNull
    @Size(max = ApiKeyHelper.API_KEY_LENGTH)
    private String apiKeyHash;

    @NotNull
    private String keyName;

    @NotNull
    private LocalDateTime expirationDate;

    @NotNull
    private LocalDateTime creationDate;

    @Size(max = 300)
    private String description;

    @Column(columnDefinition = "TINYINT(1)")
    @Builder.Default
    private Boolean removed = Boolean.FALSE;

    @NotNull
    @OneToOne
    private UserEntity user;

}
