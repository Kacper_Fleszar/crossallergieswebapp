package pl.lodz.p.entities;

import lombok.*;
import pl.lodz.p.enums.FoodAllergens;
import pl.lodz.p.enums.OtherAllergens;
import pl.lodz.p.enums.PollenSubType;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class CrossAllergensEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.PRIVATE)
    private Long crossAllergensId;

    @Enumerated(EnumType.STRING)
    @NonNull
    private PollenSubType pollenSubType;

    @Enumerated(EnumType.STRING)
    @ElementCollection(targetClass = PollenSubType.class, fetch = FetchType.LAZY)
    private Set<PollenSubType> pollenAllergens;

    @Enumerated(EnumType.STRING)
    @ElementCollection(targetClass = FoodAllergens.class, fetch = FetchType.LAZY)
    private Set<FoodAllergens> foodAllergens;

    @Enumerated(EnumType.STRING)
    @ElementCollection(targetClass = OtherAllergens.class, fetch = FetchType.LAZY)
    private Set<OtherAllergens> otherAllergens;

}
