package pl.lodz.p.entities;

import pl.lodz.p.enums.FoodAllergens;
import pl.lodz.p.enums.OtherAllergens;
import pl.lodz.p.enums.PollenSubType;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CrossAllergensEntityBuilder {

    private CrossAllergensEntity entity;

    public CrossAllergensEntityBuilder(PollenSubType pollenSubType) {
        this.entity = new CrossAllergensEntity(pollenSubType);
    }

    public CrossAllergensEntityBuilder pollenAllergens(PollenSubType... pollenSubTypes) {
        entity.setPollenAllergens(Stream.of(pollenSubTypes).collect(Collectors.toSet()));
        return this;
    }

    public CrossAllergensEntityBuilder foodAllergens(FoodAllergens... foodAllergens) {
        entity.setFoodAllergens(Stream.of(foodAllergens).collect(Collectors.toSet()));
        return this;
    }

    public CrossAllergensEntityBuilder otherAllergens(OtherAllergens... otherAllergens) {
        entity.setOtherAllergens(Stream.of(otherAllergens).collect(Collectors.toSet()));
        return this;
    }

    public CrossAllergensEntity build() {
        return entity;
    }
}
