package pl.lodz.p.entities;

import lombok.*;
import pl.lodz.p.enums.PollenConcentration;
import pl.lodz.p.enums.PollenSubType;

import javax.persistence.*;

@Entity
@Getter
@Setter(AccessLevel.PACKAGE)
@ToString
@NoArgsConstructor
@RequiredArgsConstructor
public final class PollenConcentrationEntity implements Comparable<PollenConcentrationEntity>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.PRIVATE)
    private Long pollenConcentrationId;

    @Enumerated(EnumType.STRING)
    @NonNull
    private PollenSubType pollenSubType;

    @NonNull
    private Integer weekNumber;

    @Enumerated(EnumType.STRING)
    @NonNull
    private PollenConcentration pollenConcentration;

    @Override
    public int compareTo(PollenConcentrationEntity o) {
        return pollenConcentrationId.compareTo(o.pollenConcentrationId);
    }
}
