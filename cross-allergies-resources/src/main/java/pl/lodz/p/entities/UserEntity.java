package pl.lodz.p.entities;

import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import pl.lodz.p.enums.Roles;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@SQLDelete(sql = "UPDATE user_entity SET removed = 1 WHERE user_id = ?")
@Where(clause = "removed = 0")
@Entity
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long userId;

    @NotNull
    @Size(min = 3, max = 32)
    private String username;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    @Email
    private String email;

    @NotNull
    private String password;

    @Enumerated(value = EnumType.STRING)
    private Roles role;

    @Column(columnDefinition = "TINYINT(1)")
    private Boolean removed = Boolean.FALSE;
<<<<<<< HEAD

}
=======
}
>>>>>>> 1ae8df29a6d72c72e1e089ab4d1b9fc54172653b
