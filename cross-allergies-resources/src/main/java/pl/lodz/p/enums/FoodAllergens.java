package pl.lodz.p.enums;

public enum FoodAllergens implements Internationalizable, Allergens{

    APPLE("resource.food.allergens.apple"),
    PEAR("resource.food.allergens.pear"),
    PLUM("resource.food.allergens.plum"),
    PEACH("resource.food.allergens.peach"),
    BANANA("resource.food.allergens.banana"),
    KIWI("resource.food.allergens.kiwi"),
    LITCHI("resource.food.allergens.litchi"),
    MANGO("resource.food.allergens.mango"),
    MELON("resource.food.allergens.melon"),
    ORANGE("resource.food.allergens.orange"),
    PAPAYA("resource.food.allergens.papaya"),
    AVOCADO("resource.food.allergens.avocado"),
    PEA("resource.food.allergens.pea"),
    CUCUMBER("resource.food.allergens.cucumber"),
    CARROT("resource.food.allergens.carrot"),
    POTATO("resource.food.allergens.potato"),
    CELERY("resource.food.allergens.celery"),
    SOY("resource.food.allergens.soy"),
    TOMATO("resource.food.allergens.tomato"),
    ONION("resource.food.allergens.onion"),
    ANISE("resource.food.allergens.anise"),
    CURRY("resource.food.allergens.curry"),
    DILL("resource.food.allergens.dill"),
    PAPRIKA("resource.food.allergens.paprika"),
    PEPPER("resource.food.allergens.pepper"),
    CUMIN("resource.food.allergens.cumin"),
    CORIANDER("resource.food.allergens.coriander"),
    PEANUT("resource.food.allergens.peanut"),
    NUT("resource.food.allergens.nut"),
    CHESTNUT("resource.food.allergens.chestnut"),
    FLOURS("resource.food.allergens.flours"),
    RICE("resource.food.allergens.rice"),
    SESAME("resource.food.allergens.sesame"),
    POPPY_SEED("resource.food.allergens.poppy.seed"),
    CRUSTACEANS("resource.food.allergens.crustaceans"),
    PORK("resource.food.allergens.pork"),
    GELATIN("resource.food.allergens.gelatin"),
    CHICKEN_EGG("resource.food.allergens.chicken.egg");

    private String resourceName;

    FoodAllergens(String resourceName) {
        this.resourceName = resourceName;
    }

    @Override
    public String getResourceName() {
        return resourceName;
    }
}
