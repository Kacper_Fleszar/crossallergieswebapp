package pl.lodz.p.enums;

interface Internationalizable {

    String getResourceName();
}
