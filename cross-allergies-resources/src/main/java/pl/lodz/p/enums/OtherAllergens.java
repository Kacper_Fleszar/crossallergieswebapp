package pl.lodz.p.enums;

public enum OtherAllergens implements Internationalizable, Allergens {

    LATEX("resource.other.allergens.latex"),
    PLASMA_SUBSTITUTE("resource.other.allergens.plasma.substitute"),
    FICUS("resource.other.allergens.ficus"),
    CAT_FUR("resource.other.allergens.cat.fur"),
    FEATHER("resource.other.allergens.feather");

    private String resourceName;

    OtherAllergens(String resourceName) {
        this.resourceName = resourceName;
    }

    @Override
    public String getResourceName() {
        return resourceName;
    }
}