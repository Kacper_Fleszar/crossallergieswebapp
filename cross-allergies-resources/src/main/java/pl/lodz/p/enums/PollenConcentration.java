package pl.lodz.p.enums;

public enum PollenConcentration implements Internationalizable{

    NONE(0, "resource.pollen.concentration.none"),
    SMALL(1, "resource.pollen.concentration.small"),
    MEDIUM(2, "resource.pollen.concentration.medium"),
    HIGH(3, "resource.pollen.concentration.high");

    public Integer concentrationId;

    private String resourceName;

    PollenConcentration(Integer concentrationId, String resourceName) {
        this.concentrationId = concentrationId;
        this.resourceName = resourceName;
    }

    @Override
    public String getResourceName() {
        return resourceName;
    }
}
