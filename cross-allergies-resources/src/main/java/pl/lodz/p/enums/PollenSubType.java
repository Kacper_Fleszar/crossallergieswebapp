package pl.lodz.p.enums;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public enum PollenSubType implements Internationalizable, Allergens {

    HAZEL(PollenType.TREE, "resource.pollen.subtype.hazel"),
    ALDER(PollenType.TREE, "resource.pollen.subtype.alder"),
    WILLOW(PollenType.TREE, "resource.pollen.subtype.willow"),
    BIRCH(PollenType.TREE, "resource.pollen.subtype.birch"),
    HORNBEAM(PollenType.TREE, "resource.pollen.subtype.hornbeam"),
    POPLAR(PollenType.TREE, "resource.pollen.subtype.poplar"),
    OAK(PollenType.TREE, "resource.pollen.subtype.oak"),
    GRASS(PollenType.GRASS, "resource.pollen.subtype.grass"),
    RYE(PollenType.GRASS, "resource.pollen.subtype.rye"),
    NETTLE(PollenType.GRASS, "resource.pollen.subtype.nettle"),
    LINDEN(PollenType.TREE, "resource.pollen.subtype.linden");

    private PollenType pollenType;

    private String resourceName;

    PollenSubType(PollenType pollenType, String resourceName) {
        this.pollenType = pollenType;
        this.resourceName = resourceName;
    }

    public static Set<PollenSubType> getByPollenType(final PollenType pollenTypeToFilter) {
        return Arrays.stream(PollenSubType.values())
                .filter(pst -> pollenTypeToFilter.equals(pst.pollenType))
                .collect(Collectors.toSet());
    }

    @Override
    public String getResourceName() {
        return resourceName;
    }

    public PollenType getPollenType() {
        return pollenType;
    }
}
