package pl.lodz.p.enums;

public enum PollenType implements Internationalizable {

    TREE("resource.pollen.type.tree"),
    GRASS("resource.pollen.type.grass"),
    OTHER("resource.pollen.type.other");

    private String resourceName;

    PollenType(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getResourceName() {
        return resourceName;
    }
}
