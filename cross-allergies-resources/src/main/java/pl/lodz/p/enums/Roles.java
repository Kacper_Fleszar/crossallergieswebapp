package pl.lodz.p.enums;

public enum Roles {

    USER("USER"),
    ADMIN("ADMIN");

    String roleName;

    Roles(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return this.roleName;
    }

    @Override
    public String toString() {
        return getRoleName();
    }
}
