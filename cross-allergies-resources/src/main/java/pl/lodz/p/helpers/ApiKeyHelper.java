package pl.lodz.p.helpers;

import org.springframework.security.oauth2.common.util.RandomValueStringGenerator;
import pl.lodz.p.entities.ApiKeyEntity;

import java.time.LocalDateTime;
import java.util.Optional;

public class ApiKeyHelper {

    /**
     * API Key length
     */
    public static final int API_KEY_LENGTH = 64;

    /**
     * Check if API Key expiration date is before now
     * @return true if API Key expiration date is before now, false otherwise
     */
    public static boolean isExpired(ApiKeyEntity apiKeyEntity) {
        return LocalDateTime.now().isAfter(apiKeyEntity.getExpirationDate().plusDays(1));
    }

    public static String generateHash() {
        return new RandomValueStringGenerator(ApiKeyHelper.API_KEY_LENGTH).generate();
    }
}
