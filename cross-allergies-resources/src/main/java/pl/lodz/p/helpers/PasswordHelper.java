package pl.lodz.p.helpers;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.function.Function;

public class PasswordHelper {

    public static Function<String, String> encodePassword() {
        return passwordToEncode -> new BCryptPasswordEncoder().encode(passwordToEncode);
    }
}
