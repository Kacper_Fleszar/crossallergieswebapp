package pl.lodz.p.helpers;

import java.time.LocalDate;

public class WeekNumberHelper {

    private static final int MONTH_PARTS = 4;

    /**
     * Get week number, where each month has exactly 4 equal weeks (parts),
     * i.e January has 31 days, 3 parts with 8 days and 4th with 7 days;
     * February has 4 parts, 7 days each, but leap year 1st part has 8 days </br>
     * Week number range is \<0, 47\>
     * @param localDate {@link LocalDate} date to retrieve year part
     * @return week number for given date
     */
    public static Integer getWeekNumberByLocalDate(final LocalDate localDate) {
        if (localDate == null) {
            return null;
        }
        final Integer monthWeekPart = getMonthPartByLocalDate(localDate);
        return (localDate.getMonth().getValue()-1)*MONTH_PARTS + monthWeekPart;
    }

    static Integer getMonthPartByLocalDate(final LocalDate localDate) {
        final int dayOfMonth = localDate.getDayOfMonth();
        final int monthLength = localDate.getMonth().length(localDate.isLeapYear());
        final double weekNumber = Math.ceil((double) monthLength / MONTH_PARTS);
        return (int) ((dayOfMonth-1)/weekNumber);
    }

}
