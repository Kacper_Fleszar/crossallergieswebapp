package pl.lodz.p.repositories;

<<<<<<< HEAD
        import org.springframework.data.repository.CrudRepository;
        import org.springframework.data.repository.PagingAndSortingRepository;
        import org.springframework.stereotype.Repository;
        import pl.lodz.p.entities.ApiKeyEntity;

        import java.util.Optional;
=======
import org.springframework.data.repository.Repository;
import pl.lodz.p.entities.ApiKeyEntity;
import pl.lodz.p.entities.UserEntity;

import java.util.List;
import java.util.Optional;
>>>>>>> 1ae8df29a6d72c72e1e089ab4d1b9fc54172653b

/**
 * TODO
 */
public interface ApiKeysRepository extends Repository<ApiKeyEntity, Long> {

    /**
     * Get {@link ApiKeyEntity API key} by key name and {@link UserEntity user}
     *
     * @param keyName Name of the key
     * @param user    user entity, which can be retrieved from {@link UserRepository}
     * @return Optional with {@link ApiKeyEntity API key} if found, empty optional otherwise
     */
    Optional<ApiKeyEntity> findByKeyNameAndUser(String keyName, UserEntity user);

    /**
     * TODO
     *
     * @param apiKeyHash
     * @return
     */
    Optional<ApiKeyEntity> findByApiKeyHash(String apiKeyHash);

    /**
     * TODO
     *
     * @param userEntity
     * @return
     */
    List<ApiKeyEntity> findByUser(UserEntity userEntity);

    /**
     * TODO
     * @param apiKeyId
     */
    void deleteById(Long apiKeyId);

    /**
     *
     * @param entity
     * @return
     */
    ApiKeyEntity save(ApiKeyEntity entity);




}
