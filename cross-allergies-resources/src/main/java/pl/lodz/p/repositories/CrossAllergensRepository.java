package pl.lodz.p.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.lodz.p.entities.CrossAllergensEntity;
import pl.lodz.p.enums.PollenSubType;

import java.util.List;

@Repository
public interface CrossAllergensRepository extends CrudRepository<CrossAllergensEntity, Long> {

    List<CrossAllergensEntity> findByPollenSubType(PollenSubType pollenSubType);
}
