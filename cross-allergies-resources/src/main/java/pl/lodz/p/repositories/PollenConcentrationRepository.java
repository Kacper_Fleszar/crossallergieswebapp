package pl.lodz.p.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.lodz.p.entities.PollenConcentrationEntity;
import pl.lodz.p.enums.PollenSubType;

import java.util.Set;

@Repository
public interface PollenConcentrationRepository extends CrudRepository<PollenConcentrationEntity, Long> {

    Set<PollenConcentrationEntity> findByPollenSubType(PollenSubType pollenSubType);

    Set<PollenConcentrationEntity> findByWeekNumber(Integer weekNumber);

    Set<PollenConcentrationEntity> findByPollenSubTypeAndWeekNumber(PollenSubType pollenSubType, Integer weekNumber);

}
