package pl.lodz.p.converters;

import org.junit.Assert;
import org.junit.Test;
import pl.lodz.p.enums.PollenSubType;

public class PollenSubTypeConverterTest {

    private static final String SUBTYPE_TO_CONVERT = "hazel";

    @Test
    public void testConvert() {
        PollenSubTypeConverter converter = new PollenSubTypeConverter();
        Assert.assertEquals("PollenSubType converter fails", PollenSubType.HAZEL, converter.convert(SUBTYPE_TO_CONVERT));
    }
}
