package pl.lodz.p.converters;

import org.junit.Assert;
import org.junit.Test;
import pl.lodz.p.enums.PollenType;

public class PollenTypeConverterTest {

    private static final String TYPE_TO_CONVERT = "tree";

    @Test
    public void testConvert() {
        PollenTypeConverter converter = new PollenTypeConverter();
        Assert.assertEquals("PollenType converter fails", PollenType.TREE, converter.convert(TYPE_TO_CONVERT));
    }
}
