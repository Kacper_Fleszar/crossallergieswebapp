package pl.lodz.p.helpers;

import org.junit.Assert;
import org.junit.Test;
import pl.lodz.p.entities.ApiKeyEntity;

import java.time.LocalDateTime;

public class ApiKeyHelperTest {

    @Test
    public void testExpiration() {
        ApiKeyEntity apiKeyEntity = ApiKeyEntity.builder()
                .keyName("testKey")
                .expirationDate(LocalDateTime.now().minusDays(1))
                .build();

        Assert.assertTrue("ApiKeyHelper.isExpired doesn't work", ApiKeyHelper.isExpired(apiKeyEntity));
    }
}
