package pl.lodz.p.helpers;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;

public class WeekNumberHelperTest {

    private LocalDate beginningOfYear = LocalDate.of(2019, 1, 1);
    private LocalDate endOfYear = LocalDate.of(2019, 12, 31);
    private LocalDate endOfApril = LocalDate.of(2019, Month.APRIL, Month.APRIL.length(false));
    private LocalDate endOfFebruary = LocalDate.of(2019, Month.FEBRUARY, Month.FEBRUARY.length(false));
    private LocalDate endOfSecondAprilPart = LocalDate.of(2019, Month.APRIL, 16);
    private LocalDate beginningOfThirdAprilPart = LocalDate.of(2019, Month.APRIL, 17);

    @Test
    public void getYearWeekPartByLocalDateTest() {
        Assert.assertEquals(new Integer(0), WeekNumberHelper.getWeekNumberByLocalDate(beginningOfYear));
        Assert.assertEquals(new Integer(47), WeekNumberHelper.getWeekNumberByLocalDate(endOfYear));
        Assert.assertEquals(new Integer(15), WeekNumberHelper.getWeekNumberByLocalDate(endOfApril));
        Assert.assertEquals(new Integer(7), WeekNumberHelper.getWeekNumberByLocalDate(endOfFebruary));
        Assert.assertEquals(new Integer(13), WeekNumberHelper.getWeekNumberByLocalDate(endOfSecondAprilPart));
        Assert.assertEquals(new Integer(14), WeekNumberHelper.getWeekNumberByLocalDate(beginningOfThirdAprilPart));
    }

    @Test
    public void getWeekNumberByLocalDateTest() {
        Assert.assertEquals(new Integer(0), WeekNumberHelper.getMonthPartByLocalDate(beginningOfYear));
        Assert.assertEquals(new Integer(3), WeekNumberHelper.getMonthPartByLocalDate(endOfYear));
        Assert.assertEquals(new Integer(3), WeekNumberHelper.getMonthPartByLocalDate(endOfApril));
        Assert.assertEquals(new Integer(3), WeekNumberHelper.getMonthPartByLocalDate(endOfFebruary));
        Assert.assertEquals(new Integer(1), WeekNumberHelper.getMonthPartByLocalDate(endOfSecondAprilPart));
        Assert.assertEquals(new Integer(2), WeekNumberHelper.getMonthPartByLocalDate(beginningOfThirdAprilPart));
    }

}
