package pl.lodz.p;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("pl.lodz.p.repositories")
public class CrossAllergiesRestRepositoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrossAllergiesRestRepositoryApplication.class, args);
	}

}
