package pl.lodz.p.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.service.Tags;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class Swagger2Config {

    private static String API_BASE_PACKAGE = "pl.lodz.p.rest";
    private static String LATEST_SPECIFICATION_VERSION = "LATEST";

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(LATEST_SPECIFICATION_VERSION)
                .select()
                .apis(RequestHandlerSelectors.basePackage(API_BASE_PACKAGE))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo())
                .tags(SwaggerTags.first(), SwaggerTags.remaining());
    }

    // TODO: get information from pom.xml
    private ApiInfo apiInfo() {
        ApiInfoBuilder apiInfoBuilder = new ApiInfoBuilder();
        return apiInfoBuilder
                .title("Cross Allergies REST API")
                .description("REST API documentation for endpoints of Cross Allergies Service")
                .version(LATEST_SPECIFICATION_VERSION)
                // TODO: get address from resources
                .contact(new Contact("Kacper Fleszar", "http://localhost:8080/home", "kacper.fleszar@gmail.com"))
                .build();
    }
}
