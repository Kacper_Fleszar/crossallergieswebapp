package pl.lodz.p.configuration;

import springfox.documentation.service.Tag;

import java.util.HashSet;
import java.util.Set;

public final class SwaggerTags {

    public static final String POLLEN_CONCENTRATION_TAG = "Pollen Concentration";
    private static final String POLLEN_CONCENTRATION_DESCRIPTION = "Retrieve pollen concentration";

    public static final String POLLEN_DICTIONARIES_TAG = "Pollen Dictionaries";
    private static final String POLLEN_DICTIONARIES_DESCRIPTION = "Retrieve pollen dictionary values";

    public static final String CROSS_ALLERGENS_TAG = "Cross Allergens";
    private static final String CROSS_ALLERGENS_DESCRIPTION = "Retrieve cross allergens";

    static Tag first() {
        return new Tag(POLLEN_CONCENTRATION_TAG, POLLEN_CONCENTRATION_DESCRIPTION, 1);
    }

    static Tag[] remaining() {
        Set<Tag> tags = new HashSet<>();
        tags.add(new Tag(POLLEN_DICTIONARIES_TAG, POLLEN_DICTIONARIES_DESCRIPTION));
        tags.add(new Tag(CROSS_ALLERGENS_TAG, CROSS_ALLERGENS_DESCRIPTION));
        return tags.toArray(new Tag[tags.size()]);
    }


}
