package pl.lodz.p.loader;

import pl.lodz.p.entities.CrossAllergensEntity;
import pl.lodz.p.entities.CrossAllergensEntityBuilder;
import pl.lodz.p.enums.FoodAllergens;
import pl.lodz.p.enums.OtherAllergens;
import pl.lodz.p.enums.PollenSubType;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class CrossAllergensContainerFill {

    private static List<CrossAllergensEntity> crossAllergensEntities = new ArrayList<>();

    public static List<CrossAllergensEntity> generateContainer() {
        generateHazelAllergens();
        generateAlderAllergens();
        generateWillowAllergens();
        generateBirchAllergens();
        generateHornbeamAllergens();
        generatePoplarAllergens();
        generateOakAllergens();
        generateGrassAllergens();
        generateRyeAllergens();
        generateNettleAllergens();
        generateLindenAllergens();
        return crossAllergensEntities;
    }

    private static void generateHazelAllergens() {
        crossAllergensEntities.add(new CrossAllergensEntityBuilder(PollenSubType.HAZEL)
                .foodAllergens(FoodAllergens.NUT)
                .pollenAllergens(PollenSubType.BIRCH, PollenSubType.OAK, PollenSubType.ALDER)
                .build());
    }

    private static void generateAlderAllergens() {
        crossAllergensEntities.add(new CrossAllergensEntityBuilder(PollenSubType.ALDER)
                .pollenAllergens(PollenSubType.BIRCH, PollenSubType.HAZEL)
                .build());
    }

    private static void generateWillowAllergens() {
        crossAllergensEntities.add(new CrossAllergensEntityBuilder(PollenSubType.WILLOW)
                .pollenAllergens(PollenSubType.POPLAR)
                .build());
    }

    private static void generateBirchAllergens() {
        PollenSubType[] pollen = Stream.of(PollenSubType.values())
                .filter(subType -> subType != PollenSubType.WILLOW)
                .toArray(PollenSubType[]::new);

        FoodAllergens[] foodAllergens = Stream.of(FoodAllergens.values())
                .filter(food -> food != FoodAllergens.MELON)
                .filter(food -> food != FoodAllergens.PAPAYA)
                .filter(food -> food != FoodAllergens.AVOCADO)
                .filter(food -> food != FoodAllergens.PEA)
                .filter(food -> food != FoodAllergens.CUCUMBER)
                .filter(food -> food != FoodAllergens.ONION)
                .filter(food -> food != FoodAllergens.DILL)
                .filter(food -> food != FoodAllergens.PEANUT)
                .filter(food -> food != FoodAllergens.CHESTNUT)
                .filter(food -> food != FoodAllergens.FLOURS)
                .filter(food -> food != FoodAllergens.SESAME)
                .filter(food -> food != FoodAllergens.CRUSTACEANS)
                .filter(food -> food != FoodAllergens.PORK)
                .filter(food -> food != FoodAllergens.GELATIN)
                .filter(food -> food != FoodAllergens.CHICKEN_EGG)
                .toArray(FoodAllergens[]::new);

        crossAllergensEntities.add(new CrossAllergensEntityBuilder(PollenSubType.BIRCH)
                .pollenAllergens(pollen)
                .foodAllergens(foodAllergens)
                .otherAllergens(OtherAllergens.LATEX)
                .build());
    }

    private static void generateHornbeamAllergens() {
        crossAllergensEntities.add(new CrossAllergensEntityBuilder(PollenSubType.HORNBEAM)
                .build());
    }

    private static void generatePoplarAllergens() {
        crossAllergensEntities.add(new CrossAllergensEntityBuilder(PollenSubType.POPLAR)
                .pollenAllergens(PollenSubType.WILLOW)
                .build());
    }

    private static void generateOakAllergens() {
        crossAllergensEntities.add(new CrossAllergensEntityBuilder(PollenSubType.OAK)
                .pollenAllergens(PollenSubType.BIRCH)
                .build());
    }

    private static void generateGrassAllergens() {
        crossAllergensEntities.add(new CrossAllergensEntityBuilder(PollenSubType.GRASS)
                .pollenAllergens(PollenSubType.BIRCH)
                .otherAllergens(OtherAllergens.LATEX)
                .foodAllergens(FoodAllergens.APPLE, FoodAllergens.PEAR, FoodAllergens.PEACH,
                        FoodAllergens.PLUM, FoodAllergens.KIWI, FoodAllergens.MELON, FoodAllergens.CELERY,
                        FoodAllergens.TOMATO, FoodAllergens.ONION, FoodAllergens.FLOURS, FoodAllergens.RICE)
                .build());
    }

    private static void generateRyeAllergens() {
        crossAllergensEntities.add(new CrossAllergensEntityBuilder(PollenSubType.RYE)
                .pollenAllergens(PollenSubType.BIRCH)
                .otherAllergens(OtherAllergens.LATEX)
                .foodAllergens(FoodAllergens.APPLE, FoodAllergens.PEAR, FoodAllergens.PEACH,
                        FoodAllergens.PLUM, FoodAllergens.KIWI, FoodAllergens.MELON, FoodAllergens.CELERY,
                        FoodAllergens.TOMATO, FoodAllergens.ONION, FoodAllergens.FLOURS, FoodAllergens.RICE)
                .build());
    }

    private static void generateNettleAllergens() {
        crossAllergensEntities.add(new CrossAllergensEntityBuilder(PollenSubType.NETTLE)
                .build());
    }

    private static void generateLindenAllergens() {
        crossAllergensEntities.add(new CrossAllergensEntityBuilder(PollenSubType.LINDEN)
                .build());
    }

}
