package pl.lodz.p.loader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.lodz.p.services.CrossAllergensService;
import pl.lodz.p.services.PollenConcentrationService;

import javax.annotation.PostConstruct;

// @Component
@Deprecated
public class DatabaseLoader {

    @Autowired
    PollenConcentrationService pollenConcentrationService;

    @Autowired
    CrossAllergensService crossAllergensService;

    @PostConstruct
    public void loadData() {
//        List<CrossAllergensEntity> container = CrossAllergensContainerFill.generateContainer();
//        crossAllergensService.saveAll(container);
//        List<PollenConcentrationEntity> container = PollenConcentrationContainerFill.generateEntities();
//        pollenConcentrationService.savePollenConcentration(container);
    }
}
