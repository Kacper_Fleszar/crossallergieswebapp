package pl.lodz.p.loader;

import pl.lodz.p.enums.PollenConcentration;
import pl.lodz.p.entities.PollenConcentrationEntity;
import pl.lodz.p.enums.PollenSubType;

import java.util.ArrayList;
import java.util.List;

public class PollenConcentrationContainerFill {

    private static List<PollenConcentrationEntity> entities = new ArrayList<>();

    public static List<PollenConcentrationEntity> generateEntities() {
        generateHazelData();
        generateAlderData();
        generateWillowData();
        generateBirchData();
        generateHornbeamData();
        generatePopplarData();
        generateOakData();
        generateGrassData();
        generateRyeData();
        generateNettleData();
        generateLindenData();
        return entities;
    }

    private static void generateHazelData() {
        entities.add(new PollenConcentrationEntity(PollenSubType.HAZEL, 0, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.HAZEL, 1, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.HAZEL, 2, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.HAZEL, 3, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.HAZEL, 4, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.HAZEL, 5, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.HAZEL, 6, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.HAZEL, 7, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.HAZEL, 8, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.HAZEL, 9, PollenConcentration.SMALL));
    }

    private static void generateAlderData() {
        entities.add(new PollenConcentrationEntity(PollenSubType.ALDER, 1, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.ALDER, 2, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.ALDER, 3, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.ALDER, 4, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.ALDER, 5, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.ALDER, 6, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.ALDER, 7, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.ALDER, 8, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.ALDER, 9, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.ALDER, 10, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.ALDER, 11, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.ALDER, 12, PollenConcentration.SMALL));
    }

    private static void generateWillowData() {
        entities.add(new PollenConcentrationEntity(PollenSubType.WILLOW, 6, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.WILLOW, 7, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.WILLOW, 8, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.WILLOW, 9, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.WILLOW, 10, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.WILLOW, 11, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.WILLOW, 12, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.WILLOW, 13, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.WILLOW, 14, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.WILLOW, 15, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.WILLOW, 16, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.WILLOW, 17, PollenConcentration.SMALL));
    }

    private static void generateBirchData() {
        entities.add(new PollenConcentrationEntity(PollenSubType.BIRCH, 8, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.BIRCH, 9, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.BIRCH, 10, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.BIRCH, 11, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.BIRCH, 12, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.BIRCH, 13, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.BIRCH, 14, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.BIRCH, 15, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.BIRCH, 16, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.BIRCH, 17, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.BIRCH, 18, PollenConcentration.SMALL));
    }

    private static void generateHornbeamData() {
        entities.add(new PollenConcentrationEntity(PollenSubType.HORNBEAM, 9, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.HORNBEAM, 10, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.HORNBEAM, 11, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.HORNBEAM, 12, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.HORNBEAM, 13, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.HORNBEAM, 14, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.HORNBEAM, 15, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.HORNBEAM, 16, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.HORNBEAM, 17, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.HORNBEAM, 18, PollenConcentration.SMALL));
    }

    private static void generatePopplarData() {
        entities.add(new PollenConcentrationEntity(PollenSubType.POPLAR, 10, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.POPLAR, 11, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.POPLAR, 12, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.POPLAR, 13, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.POPLAR, 14, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.POPLAR, 15, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.POPLAR, 16, PollenConcentration.SMALL));
    }

    private static void generateOakData() {
        entities.add(new PollenConcentrationEntity(PollenSubType.OAK, 15, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.OAK, 16, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.OAK, 17, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.OAK, 18, PollenConcentration.MEDIUM));
    }

    private static void generateGrassData() {
        entities.add(new PollenConcentrationEntity(PollenSubType.GRASS, 15, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.GRASS, 16, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.GRASS, 17, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.GRASS, 18, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.GRASS, 19, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.GRASS, 20, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.GRASS, 21, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.GRASS, 22, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.GRASS, 23, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.GRASS, 24, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.GRASS, 25, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.GRASS, 26, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.GRASS, 27, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.GRASS, 28, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.GRASS, 29, PollenConcentration.SMALL));
    }

    private static void generateRyeData() {
        entities.add(new PollenConcentrationEntity(PollenSubType.RYE, 18, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.RYE, 19, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.RYE, 20, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.RYE, 21, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.RYE, 22, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.RYE, 23, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.RYE, 24, PollenConcentration.SMALL));
    }

    private static void generateNettleData() {
        entities.add(new PollenConcentrationEntity(PollenSubType.NETTLE, 19, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.NETTLE, 20, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.NETTLE, 21, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.NETTLE, 22, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.NETTLE, 23, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.NETTLE, 24, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.NETTLE, 25, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.NETTLE, 26, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.NETTLE, 27, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.NETTLE, 28, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.NETTLE, 29, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.NETTLE, 30, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.NETTLE, 31, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.NETTLE, 32, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.NETTLE, 33, PollenConcentration.SMALL));
    }

    private static void generateLindenData() {
        entities.add(new PollenConcentrationEntity(PollenSubType.LINDEN, 21, PollenConcentration.SMALL));
        entities.add(new PollenConcentrationEntity(PollenSubType.LINDEN, 22, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.LINDEN, 23, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.LINDEN, 24, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.LINDEN, 25, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.LINDEN, 26, PollenConcentration.HIGH));
        entities.add(new PollenConcentrationEntity(PollenSubType.LINDEN, 27, PollenConcentration.MEDIUM));
        entities.add(new PollenConcentrationEntity(PollenSubType.LINDEN, 28, PollenConcentration.SMALL));
    }
}
