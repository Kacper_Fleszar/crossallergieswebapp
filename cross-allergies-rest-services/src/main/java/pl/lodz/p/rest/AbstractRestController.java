package pl.lodz.p.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract class AbstractRestController {

    final Logger LOG = LoggerFactory.getLogger(getClass());

}
