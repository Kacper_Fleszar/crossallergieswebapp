package pl.lodz.p.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.lodz.p.configuration.SwaggerTags;
import pl.lodz.p.entities.CrossAllergensEntity;
import pl.lodz.p.enums.PollenSubType;
import pl.lodz.p.services.CrossAllergensService;

import java.util.List;

@RestController
@RequestMapping("/cross-allergens-api/")
@Api(tags = SwaggerTags.CROSS_ALLERGENS_TAG)
public class CrossAllergensRestController extends AbstractRestController {

    private static final String CROSS_ALLERGENS_DESC = "Endpoint to retrieve all cross allergens for given pollen type. Pollen type is required.";
    private static final String CROSS_ALLERGENS_FOR_NOW_DESC = "Endpoint to retrieve cross allergens for given pollen type for now. Pollen type is required.";

    @Autowired
    private CrossAllergensService crossAllergensService;

    @GetMapping("allergens")
    @ApiOperation(value = CROSS_ALLERGENS_DESC)
    public List<CrossAllergensEntity> getCrossAllergens(@RequestParam PollenSubType pollenSubtype) {
        return crossAllergensService.findCrossAllergensByPollenSubtype(pollenSubtype);
    }

    @GetMapping("allergens/now")
    @ApiOperation(value = CROSS_ALLERGENS_FOR_NOW_DESC)
    public List<CrossAllergensEntity> getCrossAllergensForNow() {
        return crossAllergensService.findForNow();
    }
}
