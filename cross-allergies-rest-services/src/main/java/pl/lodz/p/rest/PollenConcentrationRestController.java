package pl.lodz.p.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.lodz.p.configuration.SwaggerTags;
import pl.lodz.p.helpers.WeekNumberHelper;
import pl.lodz.p.entities.PollenConcentrationEntity;
import pl.lodz.p.enums.PollenSubType;
import pl.lodz.p.services.PollenConcentrationService;

import java.time.LocalDate;
import java.util.Set;

@RestController
@RequestMapping(value = "/pollen-api/pollen-concentration")
@Api(tags = SwaggerTags.POLLEN_CONCENTRATION_TAG)
public class PollenConcentrationRestController extends AbstractRestController {

    private static final String POLLEN_CONCENTRATION_DESC =
            "Endpoint to retrieve pollen concentration. If you don't specify pollen subtype, you will get concentration for all subtypes";
    private static final String POLLEN_CONCENTRATION_FOR_NOW_DESC =
            "Endpoint to retrieve pollen concentration for now. If you don't specify pollen subtype, you will get concentration for all subtypes";

    @Autowired
    PollenConcentrationService service;

    @GetMapping
    @ApiOperation(value = POLLEN_CONCENTRATION_DESC)
    public Set<PollenConcentrationEntity> getPollenConcentration(
            @RequestParam(required = false) PollenSubType pollenSubtype,
            @RequestParam(required = false) LocalDate date) {
        return service.findBySubTypeAndWeekNumber(pollenSubtype, WeekNumberHelper.getWeekNumberByLocalDate(date));
    }

    @GetMapping("/now")
    @ApiOperation(value = POLLEN_CONCENTRATION_FOR_NOW_DESC)
    public Set<PollenConcentrationEntity> getPollenConcentrationForNow(@RequestParam(required = false) PollenSubType pollenSubtype) {
        return service.findForNow();
    }


}
