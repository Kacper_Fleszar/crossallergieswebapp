package pl.lodz.p.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import pl.lodz.p.configuration.SwaggerTags;
import pl.lodz.p.enums.PollenSubType;
import pl.lodz.p.enums.PollenType;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/pollen-api/pollen-subtypes")
@Api(tags = SwaggerTags.POLLEN_DICTIONARIES_TAG)
public class PollenSubtypeRestController extends AbstractRestController{

    @ApiOperation("Retrieve pollen subtypes by given type or all, if type is not specified.")
    @GetMapping
    public Set<PollenSubType> getPollenSubTypes(@RequestParam(required = false) PollenType pollenType) {
        if (pollenType == null) {
            return Arrays.stream(PollenSubType.values())
                    .collect(Collectors.toSet());
        }
        return PollenSubType.getByPollenType(pollenType);
    }
}
