package pl.lodz.p.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.lodz.p.configuration.SwaggerTags;
import pl.lodz.p.enums.PollenType;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/pollen-api/pollen-types")
@Api(tags = SwaggerTags.POLLEN_DICTIONARIES_TAG)
public class PollenTypeRestController extends AbstractRestController {

    @GetMapping
    @ApiOperation(value = "Endpoint to retrieve pollen types enumeration")
    public Set<PollenType> getPollenTypes() {
        return Arrays.stream(PollenType.values())
                .collect(Collectors.toSet());
    }
}
