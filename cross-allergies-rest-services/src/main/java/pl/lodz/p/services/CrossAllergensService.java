package pl.lodz.p.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.lodz.p.entities.CrossAllergensEntity;
import pl.lodz.p.enums.PollenSubType;

import java.util.List;

public interface CrossAllergensService {

    Logger LOG = LoggerFactory.getLogger(CrossAllergensService.class);

    void saveAll(List<CrossAllergensEntity> pollenConcentrations);

    List<CrossAllergensEntity> findCrossAllergensByPollenSubtype(PollenSubType pollenSubType);

    List<CrossAllergensEntity> findForNow();

}
