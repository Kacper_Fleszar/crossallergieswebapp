package pl.lodz.p.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.lodz.p.entities.CrossAllergensEntity;
import pl.lodz.p.entities.PollenConcentrationEntity;
import pl.lodz.p.enums.PollenSubType;
import pl.lodz.p.repositories.CrossAllergensRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
public class CrossAllergensServiceImpl implements CrossAllergensService {

    @Autowired
    private CrossAllergensRepository crossAllergensRepository;

    @Autowired
    private PollenConcentrationService pollenConcentrationService;


    @Override
    public void saveAll(List<CrossAllergensEntity> pollenConcentrations) {
        crossAllergensRepository.saveAll(pollenConcentrations);
    }

    @Override
    public List<CrossAllergensEntity> findCrossAllergensByPollenSubtype(PollenSubType pollenSubType) {
        return crossAllergensRepository.findByPollenSubType(pollenSubType);
    }

    @Override
    public List<CrossAllergensEntity> findForNow() {
        return pollenConcentrationService.findForNow()
                .stream()
                .map(PollenConcentrationEntity::getPollenSubType)
                .flatMap(subType -> crossAllergensRepository.findByPollenSubType(subType).stream())
                .collect(Collectors.toList());
    }
}
