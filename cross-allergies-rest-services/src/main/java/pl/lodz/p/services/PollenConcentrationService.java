package pl.lodz.p.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.lodz.p.entities.PollenConcentrationEntity;
import pl.lodz.p.enums.PollenSubType;

import java.util.List;
import java.util.Set;

public interface PollenConcentrationService {

    Logger LOG = LoggerFactory.getLogger(PollenConcentrationService.class);

    Set<PollenConcentrationEntity> findByWeekNumber(Integer week);

    Set<PollenConcentrationEntity> findBySubTypeAndWeekNumber(PollenSubType pollenSubtype, Integer week);

    Set<PollenConcentrationEntity> findForNow();

    void savePollenConcentration(List<PollenConcentrationEntity> entities);

}
