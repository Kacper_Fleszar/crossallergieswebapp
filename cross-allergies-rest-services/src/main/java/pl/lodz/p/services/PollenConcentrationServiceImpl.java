package pl.lodz.p.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.lodz.p.entities.PollenConcentrationEntity;
import pl.lodz.p.enums.PollenSubType;
import pl.lodz.p.helpers.WeekNumberHelper;
import pl.lodz.p.repositories.PollenConcentrationRepository;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class PollenConcentrationServiceImpl implements PollenConcentrationService {

    @Autowired
    private PollenConcentrationRepository repository;

    @Override
    public Set<PollenConcentrationEntity> findByWeekNumber(Integer week) {
        return convertIterableToSet(repository.findByWeekNumber(week));
    }

    @Override
    public Set<PollenConcentrationEntity> findBySubTypeAndWeekNumber(PollenSubType pollenSubtype, Integer week) {
        if (pollenSubtype == null && week == null) {
            return convertIterableToSet(repository.findAll());
        }
        if (pollenSubtype != null && week != null) {
            return repository.findByPollenSubTypeAndWeekNumber(pollenSubtype, week);
        }
        if (pollenSubtype != null) {
            return repository.findByPollenSubType(pollenSubtype);
        }
        return repository.findByWeekNumber(week);
    }

    @Override
    public Set<PollenConcentrationEntity> findForNow() {
        return convertIterableToSet(
                repository.findByWeekNumber(WeekNumberHelper.getWeekNumberByLocalDate(LocalDate.now())));
    }

    @Override
    public void savePollenConcentration(List<PollenConcentrationEntity> entities) {
        repository.saveAll(entities);
    }

    private Set<PollenConcentrationEntity> convertIterableToSet(Iterable<PollenConcentrationEntity> iterable) {
        Set<PollenConcentrationEntity> entities = new HashSet<>();
        iterable.forEach(entities::add);
        return entities;
    }
}
