package pl.lodz.p.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;
import pl.lodz.p.entities.PollenConcentrationEntity;
import pl.lodz.p.enums.PollenConcentration;
import pl.lodz.p.enums.PollenSubType;
import pl.lodz.p.repositories.PollenConcentrationRepository;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.*;

public class PollenConcentrationServiceTest {

    private PollenConcentrationService service;

    @Before
    public void setup() {
        service = new PollenConcentrationServiceImpl();
        ReflectionTestUtils.setField(service, "repository", mockRepository());
    }

    @Test
    public void testFindByWeekNumber() {
        Set<PollenConcentrationEntity> entities = service.findByWeekNumber(1);
        Assert.assertFalse("PollenConcentrationService returns empty collection", entities.isEmpty());
    }

    private PollenConcentrationRepository mockRepository() {
        Set<PollenConcentrationEntity> entitiesMock = new HashSet<>();
        PollenConcentrationEntity entity = new PollenConcentrationEntity();
        ReflectionTestUtils.setField(entity, "pollenConcentration", PollenConcentration.MEDIUM);
        entitiesMock.add(entity);
        PollenConcentrationRepository repositoryMock = mock(PollenConcentrationRepository.class);

        when(repositoryMock.findByWeekNumber(anyInt())).thenReturn(entitiesMock);
        when(repositoryMock.findByPollenSubType(any(PollenSubType.class))).thenReturn(entitiesMock);
        when(repositoryMock.findByPollenSubTypeAndWeekNumber(any(PollenSubType.class), anyInt()))
                .thenReturn(entitiesMock);
        when(repositoryMock.findAll()).thenReturn(entitiesMock);
        return repositoryMock;
    }

}
