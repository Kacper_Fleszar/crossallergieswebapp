package pl.lodz.p.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import pl.lodz.p.entities.UserEntity;
import pl.lodz.p.repositories.UserRepository;

import java.util.Optional;

@Component
public class AuthenticationComponent {

    @Autowired
    private UserRepository userRepository;

    /**
     * Retrieve current authenticated user
     * @return Current authenticated {@link UserEntity user} retrieved from database if found, empty {@link Optional} otherwise
     */
    public Optional<UserEntity> getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken) {
            return Optional.empty();
        }
        return userRepository.findByUsername(authentication.getName());
    }
}
