package pl.lodz.p.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import pl.lodz.p.enums.Roles;
import pl.lodz.p.services.UserService;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService userService;

    /**
     * Spring Security configuration method.
     * @param http {@link HttpSecurity} object
     * @throws Exception Security configuration exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                // All HTTP requests with host:port/api/... are available only for authenticated users
                .authorizeRequests()
                .antMatchers("/api/**")
                .authenticated()
            .and()
                // All HTTP requests with host:port/admin/... are available only for admin role
                .authorizeRequests()
                .antMatchers("/admin/**")
                .hasRole(Roles.ADMIN.toString())
            .and()
                .formLogin()
                // HTTP POST request for login
                .loginPage("/login")
                // Redirect to when login error occurs
                .failureUrl("/login-failed")
                .permitAll()
            .and()
                .logout()
                // HTTP POST logout request
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                // HTTP GET request for successful logout, see LoginController class
                .logoutSuccessUrl("/logout-successful")
                .deleteCookies("JSESSIONID");
    }

    /*
     * Configure authentication provider using custom method
     */
    @Autowired
    public void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authProvider());
    }

    /*
     * I'd needed to create own user class, also I'm storing users in database, so I'd to create custom user service,
     * which is used to retrieve users from DB by username.
     *
     * Password encoder is also custom created encoder, with quite basic BCrypt algorithm provided by Spring Security,
     * but it is fully customizable
     */
    @Bean
    DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    /*
     * Basic BCrypt algorithm for securing password
     */
    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}