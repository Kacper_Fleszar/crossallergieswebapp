package pl.lodz.p.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.lodz.p.models.ApiKeyGeneratorModel;
import pl.lodz.p.services.ApiKeyService;

import javax.validation.Valid;

@Controller
public class ApiKeyGeneratorController extends BaseController {

    static final String GENERATED_API_KEY_NAME = "generatedApiKeyName";
    private static final String API_GENERATOR_TEMPLATE = "api/apiKeyGenerator";

    @Autowired
    private ApiKeyService apiKeyGeneratorService;

    @GetMapping(API_GENERATOR)
    public String apiKeysGeneratorView(Model model) {
        model.addAttribute(ApiKeysListController.KEY_MODEL, new ApiKeyGeneratorModel());
        return API_GENERATOR_TEMPLATE;
    }

    @PostMapping(API_GENERATOR)
    public String generateApiKey(@Valid @ModelAttribute(ApiKeysListController.KEY_MODEL) ApiKeyGeneratorModel key,
            BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute(ApiKeysListController.KEY_MODEL, key);
            return API_GENERATOR_TEMPLATE;
        }
        if (apiKeyGeneratorService.isApiKeyNameUnique(key)) {
            apiKeyGeneratorService.saveApiKey(key);
            redirectAttributes.addAttribute(GENERATED_API_KEY_NAME, key.getKeyName());
            return redirect(API_KEYS);
        }
        model.addAttribute("keyNotUnique", true);
        return API_GENERATOR_TEMPLATE;
    }
}
