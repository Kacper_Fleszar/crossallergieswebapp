package pl.lodz.p.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.lodz.p.models.ApiKeyModel;
import pl.lodz.p.services.ApiKeyService;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ApiKeysListController extends BaseController {

    static final String KEY_MODEL = "key";
    private static final String DELETED_KEY = "deletedKey";
    private static final String API_KEYS_TEMPLATE = "api/apiKeys";


    @Autowired
    private ApiKeyService apiKeyService;

    @GetMapping(API_KEYS)
    public String apiKeysListView(@ModelAttribute(ApiKeyGeneratorController.GENERATED_API_KEY_NAME) String apiKeyName,
                                  Model model) {
        // TODO: Add pagination
        // TODO: add sql script to startup with api keys for admin
        List<ApiKeyModel> apiKeys = new ArrayList<>();
        apiKeyService.findAll().forEach(apiKey -> apiKeys.add(new ApiKeyModel(apiKey)));
        model.addAttribute("keys", apiKeys);
        if (!apiKeyName.isEmpty()) {
            apiKeyService.getApiKeyByName(apiKeyName)
                    .ifPresent(entity -> model.addAttribute("createdKey", new ApiKeyModel(entity)));
        }
        return API_KEYS_TEMPLATE;
    }

    @PostMapping(REMOVE_KEY)
    public String removeKey(@RequestParam("keyName") String keyName, RedirectAttributes redirectAttributes) {
        apiKeyService.deleteByKeyName(keyName);
        redirectAttributes.addFlashAttribute(DELETED_KEY, keyName);
        return redirect(API_KEYS);
    }
}
