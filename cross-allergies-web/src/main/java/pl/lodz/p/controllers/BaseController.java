package pl.lodz.p.controllers;

class BaseController {

    // TODO Implement all endpoints and forward method

    static final String HOME = "/home";
    static final String REGISTER = "/register";
    static final String API_GENERATOR = "/api/api-generator";
    static final String API_KEYS = "/api/api-keys";
    static final String API_DOCS = "/api/api-docs";
    static final String REMOVE_KEY = "/api/remove-key";

    /**
     * Creates redirect String for endpoint name
     *
     * @param to endpoint name
     * @return redirect String, i.e. "redirect:/path/to/endpoint"
     */
    static String redirect(String to) {
        return "redirect:" + to;
    }
}
