package pl.lodz.p.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import pl.lodz.p.models.CrossAllergensModel;
import pl.lodz.p.models.HomeModel;
import pl.lodz.p.models.LoginModel;
import pl.lodz.p.models.PollenConcentrationModel;
import pl.lodz.p.services.CrossAllergensService;
import pl.lodz.p.services.PollenConcentrationService;
import pl.lodz.p.services.ServicesStatus;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class HomePageController extends BaseController {

    @Autowired
    private PollenConcentrationService pollenConcentrationService;

    @Autowired
    private CrossAllergensService crossAllergensService;

    @GetMapping("/")
    public String defaultView() {
        return redirect(HOME);
    }

    @GetMapping(HOME)
    public String homeView(Model model) {
        List<CrossAllergensModel> crossAllergens = crossAllergensService.findCrossAllergens();
        List<PollenConcentrationModel> pollenConcentrationModels = pollenConcentrationService.getPollenConcentrationForNow();
        HomeModel homeModel = new HomeModel();
        homeModel.setRestServiceAvailable(ServicesStatus.isServiceAvailable());

        model.addAttribute("pollens", pollenConcentrationModels);
        model.addAttribute("crossAllergens", crossAllergens.stream()
                .map(CrossAllergensModel::getResourceName)
                .collect(Collectors.toList()));
        model.addAttribute("homeModel", homeModel);
        model.addAttribute("loginModel", new LoginModel());
        return "home";
    }

    //TODO: get address from resources
    @GetMapping(API_DOCS)
    public String swagger() {
        return redirect("http://localhost:8081/swagger-ui.html#/");
    }
}
