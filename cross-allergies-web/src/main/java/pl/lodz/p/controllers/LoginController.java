package pl.lodz.p.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LoginController extends BaseController{

    private static final Logger LOG = LoggerFactory.getLogger(LoginController.class);

    @GetMapping("/login")
    public String loginView(RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("needLogin", true);
        return redirect(HOME);
    }

    @GetMapping("/login-failed")
    public String loginFailed(RedirectAttributes redirectAttributes, HttpServletRequest httpServletRequest) {
        LOG.warn("Unsuccessful login from IP: {}", httpServletRequest.getRemoteHost());
        redirectAttributes.addFlashAttribute("needLogin", true);
        redirectAttributes.addFlashAttribute("loginFailed", true);
        // TODO: handle wrong login in thymeleaf using login-failed properties
        return redirect(HOME);
    }

    @GetMapping("/logout-successful")
    public String logoutSuccessful(RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("logoutSuccessful");
        return redirect(HOME);
    }
}
