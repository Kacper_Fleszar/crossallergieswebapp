package pl.lodz.p.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.lodz.p.models.RegisterModel;
import pl.lodz.p.services.UserService;

import javax.validation.Valid;

@Controller
public class RegisterController extends BaseController{

    static final String REGISTER_TEMPLATE = "register";
    static final String REGISTER_MODEL = "registerModel";
    static final String USER_EXISTS = "userExists";

    @Autowired
    private UserService userService;

    @GetMapping(REGISTER)
    public String registerView(Model model) {
        model.addAttribute(REGISTER_MODEL, new RegisterModel());
        return "register";
    }

    @PostMapping(REGISTER)
    public String registerView(@Valid @ModelAttribute RegisterModel registerModel, BindingResult bindingResult, Model model) {
        if (registerModel.getPassword() != null && !registerModel.getPassword().equals(registerModel.getConfirmPassword())) {
            bindingResult.rejectValue("confirmPassword", "confirmPassword","Passwords must be the same");
            return REGISTER_TEMPLATE;
        }
        if (userService.isUserExists(registerModel.getLogin(), registerModel.getEmail())) {
            model.addAttribute(USER_EXISTS, true);
            return REGISTER_TEMPLATE;
        }
        if (bindingResult.hasErrors()) {
            return REGISTER_TEMPLATE;
        }
        userService.saveUser(registerModel);
        return redirect(HOME);
    }
}
