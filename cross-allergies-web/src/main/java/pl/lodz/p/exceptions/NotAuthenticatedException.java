package pl.lodz.p.exceptions;

import org.springframework.security.core.AuthenticationException;

public class NotAuthenticatedException extends AuthenticationException {

    public NotAuthenticatedException(String msg, Throwable t) {
        super(msg, t);
    }

    public NotAuthenticatedException(String msg) {
        super(msg);
    }
}
