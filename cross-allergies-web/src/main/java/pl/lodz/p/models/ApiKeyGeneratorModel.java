package pl.lodz.p.models;

import lombok.*;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
@NoArgsConstructor
public class ApiKeyGeneratorModel {

    @Length(min = 3, max = 26)
    private String keyName;

    @Length(max = 300)
    private String keyDescription;
}
