package pl.lodz.p.models;

import lombok.AllArgsConstructor;
import pl.lodz.p.entities.ApiKeyEntity;
import pl.lodz.p.helpers.ApiKeyHelper;

@AllArgsConstructor
public class ApiKeyModel {

    private ApiKeyEntity apiKeyEntity;

    public boolean isExpired() {
        return ApiKeyHelper.isExpired(apiKeyEntity);
    }

    public String getKeyName() {
        return apiKeyEntity.getKeyName();
    }

    public String getKeyHash() {
        return apiKeyEntity.getApiKeyHash();
    }

    public String getDescription() {
        return apiKeyEntity.getDescription();
    }

    public String getExpirationDate() {
        return apiKeyEntity.getExpirationDate().toLocalDate().toString();
    }

}
