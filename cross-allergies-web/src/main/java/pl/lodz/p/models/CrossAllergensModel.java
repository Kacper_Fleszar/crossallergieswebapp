package pl.lodz.p.models;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter(AccessLevel.PRIVATE)
public class CrossAllergensModel implements Comparable<CrossAllergensModel>{

    private String allergenName;

    private String resourceName;

    @Override
    public int compareTo(CrossAllergensModel o) {
        return allergenName.compareTo(o.allergenName);
    }
}
