package pl.lodz.p.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HomeModel {

    private boolean restServiceAvailable;

}
