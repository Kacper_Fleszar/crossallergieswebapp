package pl.lodz.p.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class LoginModel {

    @NotEmpty
    private String username;

    @NotEmpty
    private String password;
}
