package pl.lodz.p.models;

import lombok.*;
import pl.lodz.p.enums.PollenConcentration;
import pl.lodz.p.enums.PollenSubType;


@Getter
@Setter(AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class PollenConcentrationModel implements Comparable<PollenConcentrationModel> {

    @NonNull
    private PollenSubType pollenSubType;

    @NonNull
    private PollenConcentration pollenConcentration;

    public String getRowStyle() {
        switch (pollenConcentration) {
            case HIGH:
                return "table-danger";
            case MEDIUM:
                return "table-warning";
            case SMALL:
                return "table-success";
            default:
                return "";
        }
    }

    @Override
    public int compareTo(PollenConcentrationModel o) {
        if (pollenConcentration == o.pollenConcentration) {
            return pollenSubType.toString().compareTo(o.pollenSubType.toString());
        }
        return pollenConcentration.compareTo(o.pollenConcentration);
    }
}
