package pl.lodz.p.security;

import lombok.Getter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import pl.lodz.p.entities.UserEntity;
import pl.lodz.p.enums.Roles;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
@ToString
public class User extends org.springframework.security.core.userdetails.User {

    public User(UserEntity entity) {
        super(entity.getUsername(), entity.getPassword(), true, true, true, true, getAuthoritiesByRole(entity.getRole()));
        this.firstName = entity.getFirstName();
        this.lastName = entity.getLastName();
        this.email = entity.getEmail();
    }

    private String firstName;

    private String lastName;

    private String email;

    private static List<GrantedAuthority> getAuthoritiesByRole(Roles role) {
        return Stream.of(new SimpleGrantedAuthority(role.toString()))
                .collect(Collectors.toList());
    }
}
