package pl.lodz.p.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.lodz.p.entities.ApiKeyEntity;
import pl.lodz.p.models.ApiKeyGeneratorModel;

import java.util.List;
import java.util.Optional;

@Service
public interface ApiKeyService {

    Logger LOG = LoggerFactory.getLogger(ApiKeyService.class);

    /**
     * Find all {@link ApiKeyEntity keys} for authenticated user
     * @return List of api keys
     */
    List<ApiKeyEntity> findAll();

    /**
     * Remove key from database
     * @param apiKeyName key name
     */
    void deleteByKeyName(String apiKeyName);

    /**
     * Retrieve {@link ApiKeyEntity api key} from database, using name unique for user.
     * @param name Name of user
     * @return {@link ApiKeyEntity key} wrapped in optional if found, {@link Optional}.empty() otherwise
     */
    Optional<ApiKeyEntity> getApiKeyByName(String name);

    /**
     * Maps model to JPA entity and store API Key with unique name per user
     * @param apiKeyGeneratorModel Model to map to entity
     */
    void saveApiKey(ApiKeyGeneratorModel apiKeyGeneratorModel);

    /**
     * Check if API key is unique in database
     * @param apiKeyGeneratorModel {@link ApiKeyGeneratorModel Model} with key name and description
     * @return true if key is not found in database, false otherwise
     */
    boolean isApiKeyNameUnique(ApiKeyGeneratorModel apiKeyGeneratorModel);

}
