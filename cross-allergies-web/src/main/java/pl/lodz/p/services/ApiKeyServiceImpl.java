package pl.lodz.p.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.util.RandomValueStringGenerator;
import org.springframework.stereotype.Service;
import pl.lodz.p.components.AuthenticationComponent;
import pl.lodz.p.entities.ApiKeyEntity;
import pl.lodz.p.entities.UserEntity;
import pl.lodz.p.exceptions.NotAuthenticatedException;
import pl.lodz.p.helpers.ApiKeyHelper;
import pl.lodz.p.models.ApiKeyGeneratorModel;
import pl.lodz.p.repositories.ApiKeysRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ApiKeyServiceImpl implements ApiKeyService {

    @Autowired
    private AuthenticationComponent authenticationComponent;

    @Autowired
    private ApiKeysRepository apiKeysRepository;

    @Override
    public List<ApiKeyEntity> findAll() {
        List<ApiKeyEntity> keys = new ArrayList<>();
        apiKeysRepository.findByUser(getUserIfPresent())
                .forEach(keys::add);
        return keys;
    }

    @Override
    public void deleteByKeyName(String apiKeyName) {
        Optional<ApiKeyEntity> entityOptional = apiKeysRepository.findByKeyNameAndUser(apiKeyName, getUserIfPresent());
        entityOptional.ifPresent(entity -> apiKeysRepository.deleteById(entity.getApiKeyId()));
    }

    public void prolongApiKey(String apiKeyName) {
        // TODO: Implement key prolongation
    }

    @Override
    public Optional<ApiKeyEntity> getApiKeyByName(String name) {
        Optional<ApiKeyEntity> apiKeyOptional = apiKeysRepository.findByKeyNameAndUser(name, getUserIfPresent());
        if (!apiKeyOptional.isPresent()) {
            LOG.warn("API Key for name: {} is not present", name);
        }
        return apiKeyOptional;
    }

    @Override
    public void saveApiKey(ApiKeyGeneratorModel apiKeyGeneratorModel) {
        String apiKeyHash = generateUniqueHash();
        LocalDateTime creationDate = LocalDateTime.now();
        ApiKeyEntity apiKey = ApiKeyEntity.builder()
                .apiKeyHash(apiKeyHash)
                .creationDate(creationDate)
                .expirationDate(creationDate.plusYears(1))
                .keyName(apiKeyGeneratorModel.getKeyName())
                .description(apiKeyGeneratorModel.getKeyDescription())
                .user(getUserIfPresent())
                .build();
        apiKeysRepository.save(apiKey);
    }

    @Override
    public boolean isApiKeyNameUnique(ApiKeyGeneratorModel apiKeyGeneratorModel) {
        return !apiKeysRepository.findByKeyNameAndUser(apiKeyGeneratorModel.getKeyName(), getUserIfPresent()).isPresent();
    }

    private String generateUniqueHash() {
        String apiKeyHash;
        Optional<ApiKeyEntity> apiKeyHashOptional;
        do {
            apiKeyHash = ApiKeyHelper.generateHash();
            apiKeyHashOptional = apiKeysRepository.findByApiKeyHash(apiKeyHash);
        } while (apiKeyHashOptional.isPresent());
        return apiKeyHash;
    }

    private UserEntity getUserIfPresent() {
        Optional<UserEntity> authenticatedUser = authenticationComponent.getAuthenticatedUser();
        if (authenticatedUser.isPresent()) {
            return authenticatedUser.get();
        }
        LOG.debug("No user is authenticated");
        throw new NotAuthenticatedException("User is not authenticated");
    }
}
