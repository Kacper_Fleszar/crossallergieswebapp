package pl.lodz.p.services;

import org.springframework.stereotype.Service;
import pl.lodz.p.models.CrossAllergensModel;

import java.util.List;

@Service
public interface CrossAllergensService {

    List<CrossAllergensModel> findCrossAllergens();
}
