package pl.lodz.p.services;

import org.springframework.stereotype.Service;
import pl.lodz.p.entities.CrossAllergensEntity;
import pl.lodz.p.models.CrossAllergensModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CrossAllergensServiceImpl implements CrossAllergensService {

    private GetRestCrossAllergens getRestCrossAllergens = new GetRestCrossAllergens();

    @Override
    public List<CrossAllergensModel> findCrossAllergens() {
        return getRestCrossAllergens.getForNow()
                .stream()
                .map(CrossAllergensServiceImpl::getAllAllergensFromCrossAllergen)
                .flatMap(List::stream)
                .sorted()
                .collect(Collectors.toList());
    }

    private static List<CrossAllergensModel> getAllAllergensFromCrossAllergen(CrossAllergensEntity crossAllergensEntity) {
        List<CrossAllergensModel> allergens = new ArrayList<>();
        allergens.addAll(crossAllergensEntity.getFoodAllergens()
                .stream()
                .map(allergen -> new CrossAllergensModel(allergen.name(), allergen.getResourceName()))
                .collect(Collectors.toList()));
        allergens.addAll(crossAllergensEntity.getPollenAllergens()
                .stream()
                .map(allergen -> new CrossAllergensModel(allergen.name(), allergen.getResourceName()))
                .collect(Collectors.toList()));
        allergens.addAll(crossAllergensEntity.getOtherAllergens()
                .stream()
                .map(allergen -> new CrossAllergensModel(allergen.name(), allergen.getResourceName()))
                .collect(Collectors.toList()));
        return allergens;
    }
}
