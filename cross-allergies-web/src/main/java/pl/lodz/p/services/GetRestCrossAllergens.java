package pl.lodz.p.services;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import pl.lodz.p.entities.CrossAllergensEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class GetRestCrossAllergens extends GetRestResponse {

    List<CrossAllergensEntity> getForNow() {
        ResponseEntity<CrossAllergensEntity[]> responseEntity = null;
        try {
            // TODO: Get host from resources
            String url = "http://localhost:8081/cross-allergens-api/allergens/now";
            LOG.info("Requesting service: {}", url);
            responseEntity = restTemplate.getForEntity(url, CrossAllergensEntity[].class);
            ServicesStatus.setStatus(responseEntity.getStatusCode());
        } catch (RestClientException e) {
            LOG.error("Connection to REST service failed:", e);
        }
        if (responseEntity == null || responseEntity.getBody() == null) {
            return new ArrayList<>();
        }
        return Arrays.stream(responseEntity.getBody()).collect(Collectors.toList());
    }
}
