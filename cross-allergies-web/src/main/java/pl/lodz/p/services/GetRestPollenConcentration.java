package pl.lodz.p.services;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import pl.lodz.p.entities.PollenConcentrationEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class GetRestPollenConcentration extends GetRestResponse {

    /**
     * TODO: Add java doc
     *
     * @return
     */
    List<PollenConcentrationEntity> getForNow() {
        ResponseEntity<PollenConcentrationEntity[]> responseEntity = null;
        try {
            String url = "http://localhost:8081/pollen-api/pollen-concentration/now";
            // TODO: Get host from resources
            LOG.info("Requesting service: {}", url);
            responseEntity = restTemplate.getForEntity(url, PollenConcentrationEntity[].class);
            ServicesStatus.setStatus(responseEntity.getStatusCode());
        } catch (RestClientException e) {
            LOG.error("Connection to REST service failed:", e);
        }
        if (responseEntity == null || responseEntity.getBody() == null) {
            return new ArrayList<>();
        }
        return Arrays.stream(responseEntity.getBody()).collect(Collectors.toList());
    }
}
