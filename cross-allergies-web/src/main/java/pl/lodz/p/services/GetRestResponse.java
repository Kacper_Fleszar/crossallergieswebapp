package pl.lodz.p.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

abstract class GetRestResponse {

    final static Logger LOG = LoggerFactory.getLogger(GetRestResponse.class);

    RestTemplate restTemplate = new RestTemplate();

}
