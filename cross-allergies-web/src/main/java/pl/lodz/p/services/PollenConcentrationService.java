package pl.lodz.p.services;

import org.springframework.stereotype.Service;
import pl.lodz.p.models.PollenConcentrationModel;

import java.util.List;

@Service
public interface PollenConcentrationService {

    List<PollenConcentrationModel> getPollenConcentrationForNow();
}
