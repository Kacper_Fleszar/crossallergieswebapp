package pl.lodz.p.services;

import org.springframework.stereotype.Service;
import pl.lodz.p.models.PollenConcentrationModel;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PollenConcentrationServiceImpl implements PollenConcentrationService {

    private GetRestPollenConcentration getRestPollenConcentration = new GetRestPollenConcentration();

    @Override
    public List<PollenConcentrationModel> getPollenConcentrationForNow() {
        return getRestPollenConcentration.getForNow()
                .stream()
                .map(entity -> new PollenConcentrationModel(entity.getPollenSubType(), entity.getPollenConcentration()))
                .sorted()
                .collect(Collectors.toList());
    }
}
