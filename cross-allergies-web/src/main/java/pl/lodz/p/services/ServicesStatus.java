package pl.lodz.p.services;

import org.springframework.http.HttpStatus;

public class ServicesStatus {

    private static HttpStatus status;

    static void setStatus(HttpStatus status) {
        ServicesStatus.status = status;
    }

    /**
     * Check if service is available
     * TODO: add to all services
     * @return false if status is null or return code is 4xx or 3xx
     */
    public static boolean isServiceAvailable() {
        if (status == null) {
            return false;
        }
        return !status.is4xxClientError() && !status.is5xxServerError();
    }

}
