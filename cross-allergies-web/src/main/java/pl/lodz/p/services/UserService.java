package pl.lodz.p.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import pl.lodz.p.models.RegisterModel;

@Service
public interface UserService extends UserDetailsService {

    Logger LOG = LoggerFactory.getLogger(UserService.class);

    boolean isUserExists(String login, String email);

    void saveUser(RegisterModel registerModel);

}
