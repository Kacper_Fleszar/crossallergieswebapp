package pl.lodz.p.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.lodz.p.entities.UserEntity;
import pl.lodz.p.enums.Roles;
import pl.lodz.p.helpers.PasswordHelper;
import pl.lodz.p.models.RegisterModel;
import pl.lodz.p.repositories.UserRepository;
import pl.lodz.p.security.User;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public boolean isUserExists(String login, String email) {
        return userRepository.findByUsernameOrEmail(login, email).isPresent();
    }

    @Transactional
    @Override
    public void saveUser(RegisterModel registerModel) {
        String loginToCheck = registerModel.getLogin();
        String emailToCheck = registerModel.getEmail();
        if (isUserExists(loginToCheck, emailToCheck)) {
            LOG.debug("User {} already exists.", loginToCheck);
            return;
        }
        UserEntity userEntity = UserEntity.builder()
                .firstName(registerModel.getFirstName())
                .lastName(registerModel.getLastName())
                .email(registerModel.getEmail())
                .username(registerModel.getLogin())
                .password(PasswordHelper.encodePassword().apply(registerModel.getPassword()))
                .role(Roles.USER)
                .removed(false)
                .build();
        userRepository.save(userEntity);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserEntity> userEntityOptional = userRepository.findByUsername(username);
        if (!userEntityOptional.isPresent()) {
            LOG.warn("Username {} is not found", username);
            throw new UsernameNotFoundException("No user found with username: " + username);
        }
        UserEntity userEntity = userEntityOptional.get();
        return new User(userEntity);
    }
}
